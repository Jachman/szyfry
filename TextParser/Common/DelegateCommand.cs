﻿using System;
using System.Windows.Input;
using Common.Annotations;

namespace Common
{
    public class DelegateCommand : ICommand
    {
        private readonly Action _action;
        private readonly Func<bool> _canExecute;

        public DelegateCommand([CanBeNull] Action action, [CanBeNull] Func<bool> canExecute = null)
        {
            _action = action;
            _canExecute = canExecute;
        }

        public bool CanExecute([CanBeNull] object parameter)
        {
            return _canExecute?.Invoke() ?? true;
        }

        public void Execute([CanBeNull] object parameter)
        {
            _action?.Invoke();
        }

        public event EventHandler CanExecuteChanged;
    }
}
