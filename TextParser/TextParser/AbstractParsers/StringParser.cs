﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using Common.Annotations;

namespace TextParser.AbstractParsers
{
    /// <summary>
    /// Parsees file based on it's content
    /// </summary>
    public abstract class StringParser : IParser
    {
        public void ParseFile(string inputFile, string outputFile)
        {
            var input = File.ReadAllText(inputFile);
            var output = ParseText(input);
            File.WriteAllText(outputFile, output);
        }

        public IEnumerable<string> ParseAll(IEnumerable<string> input)
        {
            return input.Select(ParseText);
        }

        public abstract string ParseText(string input);
    }
}
