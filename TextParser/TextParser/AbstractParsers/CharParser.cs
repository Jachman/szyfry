﻿using System.Linq;

namespace TextParser.AbstractParsers
{
    /// <summary>
    /// Parses file char-by-char, turning every char to another char
    /// </summary>
    public abstract class CharParser : StringParser
    {
        public override string ParseText(string input)
        {
            return string.Concat(input.Select(ParseChar));
        }

        protected abstract char ParseChar(char input);
    }
}
