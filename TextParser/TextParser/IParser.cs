﻿using System.Collections.Generic;
using Common.Annotations;

namespace TextParser
{
    public interface IParser
    {
        void ParseFile([NotNull] string inputFile, [NotNull] string outputFile);

        [Pure, NotNull]
        string ParseText([NotNull] string input);

        [Pure, NotNull, ItemNotNull]
        IEnumerable<string> ParseAll([NotNull, ItemNotNull] IEnumerable<string> input);
    }
}
