﻿using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Input;
using Common;
using Microsoft.Win32;

namespace TextParser.UI
{
    public class MainViewModel : NotifyPropertyChanged
    {
        private string _sourcePath;
        private string _destinationPath;
        private IParser _currentParser;

        public MainViewModel()
        {
            SelectSourceCommand = new DelegateCommand(SelectSource);
            SelectDestinationCommand = new DelegateCommand(SelectDestination);
            StartCommand = new DelegateCommand(Start);

            var manager = new ParserManager();
            foreach(var parser in manager.Parsers.OrderBy(p=>p.ToString()))
                Parsers.Add(parser);

            CurrentParser = Parsers.FirstOrDefault();
        }

        private void Start()
        {
            CurrentParser?.ParseFile(SourcePath, DestinationPath);
        }

        private void SelectDestination()
        {
            var sfd = new SaveFileDialog();

            if (sfd.ShowDialog() == true)
            {
                DestinationPath = sfd.FileName;
            }
        }

        private void SelectSource()
        {
            var ofd = new OpenFileDialog();

            if (ofd.ShowDialog() == true)
            {
                SourcePath = ofd.FileName;
            }
        }

        public string SourcePath
        {
            get => _sourcePath;
            set
            {
                if (value == _sourcePath) return;
                _sourcePath = value;
                OnPropertyChanged();
            }
        }

        public ICommand SelectSourceCommand { get; }

        public string DestinationPath
        {
            get => _destinationPath;
            set
            {
                if (value == _destinationPath) return;
                _destinationPath = value;
                OnPropertyChanged();
            }
        }

        public ICommand SelectDestinationCommand { get; }

        public ICommand StartCommand { get; }

        public ObservableCollection<IParser> Parsers { get; } = new ObservableCollection<IParser>();

        public IParser CurrentParser
        {
            get => _currentParser;
            set
            {
                if (Equals(value, _currentParser)) return;
                _currentParser = value;
                OnPropertyChanged();
            }
        }
    }
}

