﻿using System;
using TextParser.AbstractParsers;

namespace PokemonCaseParser
{
    public class PokemonParser : CharParser
    {
        private readonly Random _rnd = new Random();

        public override string ToString()
        {
            return "PoKemOn";
        }

        protected override char ParseChar(char c)
        {
            return _rnd.Next(2) == 0 ? char.ToUpper(c) : char.ToLower(c);
        }
    }
}
